package com.example.demo;

import java.time.LocalDate;

public class CDrink {
	private String maNuocUong;
	private String tenNuocUong;
	private int donGia;
	private String ghiChu;
	private LocalDate ngayTao;
	private LocalDate ngayCapNhat;
	
	public CDrink() {
	}

	public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, LocalDate ngayTao,
			LocalDate ngayCapNhat) {
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.donGia = donGia;
		this.ghiChu = ghiChu;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	/**
	 * @return the maNuocUong
	 */
	public String getMaNuocUong() {
		return maNuocUong;
	}

	/**
	 * @param maNuocUong the maNuocUong to set
	 */
	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}

	/**
	 * @return the tenNuocUong
	 */
	public String getTenNuocUong() {
		return tenNuocUong;
	}

	/**
	 * @param tenNuocUong the tenNuocUong to set
	 */
	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}

	/**
	 * @return the donGia
	 */
	public int getDonGia() {
		return donGia;
	}

	/**
	 * @param donGia the donGia to set
	 */
	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}

	/**
	 * @return the ghiChu
	 */
	public String getGhiChu() {
		return ghiChu;
	}

	/**
	 * @param ghiChu the ghiChu to set
	 */
	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	/**
	 * @return the ngayTao
	 */
	public LocalDate getNgayTao() {
		return ngayTao;
	}

	/**
	 * @param ngayTao the ngayTao to set
	 */
	public void setNgayTao(LocalDate ngayTao) {
		this.ngayTao = ngayTao;
	}

	/**
	 * @return the ngayCapNhat
	 */
	public LocalDate getNgayCapNhat() {
		return ngayCapNhat;
	}

	/**
	 * @param ngayCapNhat the ngayCapNhat to set
	 */
	public void setNgayCapNhat(LocalDate ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}
}
