package com.example.demo;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CDrinkController {
	@CrossOrigin
	@GetMapping("/drinklist")
	public ArrayList<CDrink> getDrinkList() {
		ArrayList<CDrink> drinkList = new ArrayList<CDrink>();
		LocalDate date = LocalDate.now();
		CDrink traTac = new CDrink("TRATAC","Trà tắc", 10000, null, date, date);
		CDrink coca = new CDrink("COCA", "Cocacola", 15000, null, date, date);
		CDrink pepsi = new CDrink("PEPSI", "Pepsi", 15000, null, date, date);
		CDrink lavie = new CDrink("LAVIE", "Lavie", 5000, null, date, date);
		CDrink trasua = new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, date, date);
		CDrink fanta = new CDrink("FANTA", "Fanta", 15000, null, date, date);
		drinkList.add(traTac);
		drinkList.add(coca);
		drinkList.add(pepsi);
		drinkList.add(lavie);
		drinkList.add(trasua);
		drinkList.add(fanta);
		return drinkList;
	}
}
